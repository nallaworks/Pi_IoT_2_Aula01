# Introdução

O desenvolvimento Frontend exige alguma infra já estabelicida, nessa aula vamos montar um Backend de forma precária para podermos focar no desenvolvimento. Para pode criar as funções de CRUD, de forma simples. Para montar o nosso Backend usaremos uma aplicação nodejs, chamda JsonServer que tem o objetivo de apartir de uma Json estruturado e cria todos os métodos da API, porém não é uma plataforma estável e robusta para o ambiente de produção.

>Para Simular os serviços de backend com uma API REST usaremos o json-server.

O servidor JSON é um pacote npm que você pode criar um serviço de rede REST JSON. Tudo o que precisamos é um arquivo JSON e que será usado como nossos serviço de backend.

O Json-server é open source e está disponível no [github] (https://github.com/typicode/json-server) 


# Instalando o Json-server

Você pode instalá-lo localmente para projeto específico ou globalmente. Eu irei com o local.

```sh
$ npm install -g json-server
```
Acima é um comando de linha única para instalar o servidor json. A opção -g faz com que o pacote seja instalado como global.

Verifique a versão do servidor JSON usando json-server -v.


# O nosso arquivo Json

De acordo com a convenção padrão, vamos nomear o arquivo db.json, você pode nomeá-lo de acordo com suas necessidades. 

```json
{
  "Pizzas": [
    {
      "id": 1,
      "Pizza": "Muçarela",
      "Ingredintes": "Queijo, muçarela",
      "Preço": 26.90
    },
    {
      "id": 2,
      "Pizza": "Calabreza",
      "Ingredintes": "Queijo muçarela, Calabreza e Cebola",
      "Preço": 29.90
    }
  ],
  "Bebidas": [
      {
        "id": 1,
        "Bebida": "Coca-cola",
        "Volume": "2 Litros",
        "Preço": 8.90
      },
      {
          "id": 2,
          "Bebida": "Fanta",
          "Volume": "2 Litros",
          "Preço": 7.90
        }
    ],
    "Pedidos": [
      {
        "id": 1,
        "Pizza": "Muçarela",
        "Quantidade": "1",
        "Bebida": "Coca-cola",
        "Quantidade": 1,
        "Total": 40.80
      }
    ]
}
```
Por simplicidade, incluímos 3 grupos, Pizzas, Bebidas e Pedidos. Pizza, contém o cardápio de pizzas do restaurante, Bebida o cárdapio de Bebidas e pedido registra os pedidos, que devem conter 1 pizza e 1 bebida para fins didáticos.

# Inicie o servidor JSON

Para iniciar o Json-server use o comando abaixo.

```sh
json-server --watch db.json
```
Se tudo der certo irá aparecer os dados da API, como na imagem abaixo:

![Json server](https://gitlab.com/nallaworks/Pi_IoT_2_Aula01/raw/master/imagens/tela1.png  "Rodando!")


O servidor JSON estará em execução na porta 3000 .

# Operações de CRUD

As operações de CRUD seguem Rotas prédefinidas.

```txt
URL rotas o Cardápio da pizzaria
[GET] http://localhost:3000/Pizzas para consultar acrescente o /id/ exemplo: http://localhost:3000/Pizzas/1/
[POST] http://localhost:3000/Pizzas para inserir, POST Json com os dados
[PUT] http://localhost:3000/Pizzas/ID/  com os atributos para alteração.
[DELETE] http://localhost:3000/Pizzas/ID/ para deletar o registro do JSON
```
Mudando para as operações do CRUD. Assim podemos acessar todos o RESTfull, como Pizzas, Bebidas e Pedidos.

```txt
URL rotas o Bebidas da pizzaria
[GET] http://localhost:3000/Bebidas para consultar acrescente o /id/ exemplo: http://localhost:3000/Bebidas/1/
[POST] http://localhost:3000/PBebidas para inserir, POST Json com os dados
[PUT] http://localhost:3000/Bebidas/ID/  com os atributos para alteração.
[DELETE] http://localhost:3000/Bebidas/ID/ para deletar o registro do JSON
```
# Testando a API

Para testar a API, foi desenvolvido pequenos Scripts em Pyhton.

## Inserir nova Pizza no Cardápio

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json, requests
url = 'http://localhost:3000/Pizzas'
payload =   {
            "Pizza": "Quatro Queijos",
            "Ingredintes": "Queijo muçarela, Prato, Gorgonzola e Parmesão",
            "Preço": 39.9
            }
r = requests.post(url, data=json.dumps(payload), headers = {'Content-type': 'application/json'})
```

## Alterar Preço da Pizza no Cardápio 

Para alterar um valor é necessário enviar todos os valores, no exemplo abaixo alteramos o Preço da pizza id = 2, Calabreza

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json, requests
url = 'http://localhost:3000/Pizzas/2'
payload =   {
             "id": 2,
            "Pizza": "Calabreza",
            "Ingredintes": "Queijo muçarela, Calabreza e Cebola",
            "Preço": 49.9
           
            }
r = requests.put(url, data=json.dumps(payload), headers = {'Content-type': 'application/json'})
```

## Para deletar um registro Cardápio 

Para deletar um registro é só passar a url com id do registro, usando o DELETE.

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json, requests
url = 'http://localhost:3000/Pizzas/3'
r = requests.delete(url)
```






